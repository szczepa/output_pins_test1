#include <wiringPi.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <fstream>
#include <chrono>

bool sem1G = true;

void button_check(int button, int led, bool & sem)
{
 int val;
 int counter = 0;
 sem = true;
 bool loop = true;

 while(loop)
{
  val = digitalRead(button);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  digitalWrite(led, val);
  if(val == 0)
  {
   counter++;
   system("clear");
   std::cout << "\033[1;31m Quitting \033[0m :  " << counter * 100/10 << "%" << std::endl ; 
  }
 
  if(counter >=  10)
  {
   sem = false;
   counter = 0;
   loop = false;
  }

}
}

void blinker(int led, int time)
{
 while(true)
 {
  digitalWrite(led, 1);
  std::this_thread::sleep_for(std::chrono::milliseconds(time));
  digitalWrite(led, 0);
  std::this_thread::sleep_for(std::chrono::milliseconds(time));
 } 
}


int main()
{
 wiringPiSetup();

 digitalWrite(28, 0);

 std::fstream data_file;
 std::vector<int> output_pins;
 int pin; 

 output_pins.reserve(9);
 data_file.open("data", std::fstream::in);

 for(int i = 0; i < 10; ++i)
 {
  data_file >> pin;
  output_pins.push_back(pin); 
 }

 std::thread t1(button_check, 21, 29,std::ref(sem1G));
 std::thread t2(blinker, 27, 100 );
 std::thread t3(blinker, 4, 250);
 std::thread t4(blinker, 5, 1000);
 std::thread t5(blinker, 11, 50 );
 std::thread t6(blinker, 6, 333);

 
 std::for_each(output_pins.begin(), output_pins.end(),
 		[](int pin){
		std::cout << "Pin : " << pin << '\n';
		pinMode(pin, OUTPUT);
			}
		);

 std::cout<< "Ustawiono :" << output_pins.size() << " pinow jako wyjscie.\n"; 

 while(sem1G){};


 t1.detach();
 t2.detach(); 
 t3.detach(); 
 t4.detach();
 t5.detach();
 t6.detach();
 
 digitalWrite(22,1);
 std::this_thread::sleep_for(std::chrono::milliseconds(500));
 
 for(auto i : output_pins)
 {
  digitalWrite(i, 1);
 }
 digitalWrite(22, 0);

}
